//Databases
Articles = new Mongo.Collection('articles');
//Messages = new Mongo.Collection('messages');

if (Meteor.isClient) {
  Meteor.subscribe("articles");
  Meteor.subscribe("userData");
  //Meteor.subscribe("messages");
}

Meteor.startup(function() {
    if (Meteor.isClient) {
        return SEO.config({
            title: 'The Digital Times',
            meta: {
                'description': 'Share and publish articles about your favorite topics with the world!',
                'keywords'   : 'news, article, upload, free, post, maryland, dc, opinion'
            },
            og: {
                'image': 'http://thedigitaltimes.com/img/logo.png'
           }
        });
    }
});

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    /*Accounts.emailTemplates.from='no-reply@thedigitaltimes.com';
    Accounts.emailTemplates.sitename='The Digital Times';

    Accounts.config({
        sendVerificationEmail: true
    });*/
  });

  Meteor.publish("articles", function(){
    return Articles.find({});
  });

  Meteor.publish("userData", function(){
    return Meteor.users.find({});
  });

  Meteor.publish("articlesInfinite", function(limit){
    if (limit > Articles.find().count()) {
      limit = 0;
    }

    return Articles.find({}, {limit: limit});
  });

  Meteor.publish("articlesSubject", function(limit, subject){
    if (limit > Articles.find().count()) {
      limit = 0;
    }
    return Articles.find({subjectType: subject, draft: false}, {sort: {hotness: -1}, limit: limit});
  });

  Meteor.publish("articlesPage", function(currentArticleId, subject){
    return Articles.find({_id: currentArticleId, subjectType: subject, draft: false});
  });

  Meteor.publish("articlesUser", function(pageUser, source){
    if (source == "twitter") { //return articles for twitter profile
      var user = Meteor.users.find({"services.twitter.screenName": pageUser});
      var owner = user.map(function (person) {
          return person._id;
      });
      return Articles.find({users: owner, draft: false}, {sort: {createdAt: -1}});
    } else if (source == "facebook") {
      var user = Meteor.users.find({"services.facebook.email": pageUser});
      var owner = user.map(function (person) {
          return person._id;
      });
      return Articles.find({users: owner, draft: false}, {sort: {createdAt: -1}});
    } else {
      var user = Meteor.users.find({username: pageUser});
      var owner = user.map(function (person) {
          return person._id;
      });
      return Articles.find({users: owner, draft: false}, {sort: {createdAt: -1}});
    }
  });

  Meteor.publish("userPage", function(pageUser, source){
    if (source == "twitter") { //return articles for twitter profile
      return Meteor.users.find({"services.twitter.screenName": pageUser});
    } else if (source == "facebook") {
      return Meteor.users.find({"services.facebook.email": pageUser});
    } else {
      return Meteor.users.find({username: pageUser});
    }
  });
  /*Meteor.publish("messages", function(){
    return Messages.find({}, {sort: {ts: -1}});
  });*/

}
