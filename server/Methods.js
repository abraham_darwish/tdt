Meteor.methods({
	sendEmail: function(name, email, message){

		this.unblock();
		Email.send({
			to: 'info@tdtimes.net',
			from: email,
			subject: 'Contact Page:',
			text: message,
		})
	},
	likePage: function(currentArticleId){
		if (Articles.find({_id: currentArticleId, users: Meteor.userId()}).count() > 0) {
			Articles.update({_id: currentArticleId}, {$pull:{users: Meteor.userId()}, $inc: {likes: -1}});
		}
		else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
			if (Articles.find({_id: currentArticleId, usersDis: Meteor.userId()}).count() > 0) {
				Articles.update({_id: currentArticleId}, {$pull:{usersDis: Meteor.userId()}, $inc: {dislikes: -1}});
			}
			Articles.update({_id: currentArticleId}, {$push:{users: Meteor.userId()}, $inc: {likes: 1}});
		};
	},
	dislikePage: function(currentArticleId){
		if (Articles.find({_id: currentArticleId, usersDis: Meteor.userId()}).count() > 0) {
			Articles.update({_id: currentArticleId}, {$pull:{usersDis: Meteor.userId()}, $inc: {dislikes: -1}});
		}
		else { /*If user didn't dislike before add user, remove like, increment dislike counter by 1 and say that user liked the post*/
			if (Articles.find({_id: currentArticleId, users: Meteor.userId()}).count() > 0) {
				Articles.update({_id: currentArticleId}, {$pull:{users: Meteor.userId()}, $inc: {likes: -1}});
			}
			Articles.update({_id: currentArticleId}, {$push:{usersDis: Meteor.userId()}, $inc: {dislikes: 1}});
		};
	},

	citePage: function(currentArticleId){
		if (Articles.find({_id: currentArticleId, usersCite: Meteor.userId()}).count() > 0) {
			//nothing
		}
		else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
			Articles.update({_id: currentArticleId}, {$push:{usersCite: Meteor.userId()}, $inc: {citations: 1}});
		};
	},

	userCite: function(currentArticleId){
		if (Articles.find({_id: currentArticleId, usersCite: Meteor.userId()}).count() > 0) {
			//nothing
		}
		else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
			var article = Articles.find({_id: currentArticleId});
			article.forEach(function (post) {
				Meteor.users.update(
					{_id: post.owner},

					{$inc:
						{
							"profile.citations":1,
						}
					}
				);
			});
		};
	},

	deleteArticle: function(currentArticleId){
		Articles.remove({_id:currentArticleId});
	},

	publishShared: function(category, subjectType, imglink, articleUsername, hotness, url, lat, lng){
		this.unblock();
		var owner = Meteor.userId();
		var extractor = Meteor.npmRequire('article-extractor');
		extractor.extractData(url, Meteor.bindEnvironment(function(err, data) {
			if (data) {
				Articles.insert({
			        title: data.title,
			        subtitle: data.content.substring(0, 70) + '...',
			        category: category,
			        subjectType: subjectType,
			        city: "",
			        sHTML: data.summary,
			        createdAt: new Date(),
							date: (new Date()).getTime(),
			        owner: owner,
			        articleUsername: articleUsername,
			        likes: 0,
							dislikes: 0,
			        citations: 0,
			        flags: 0,
							hotness: hotness,
			        users: [],
			        draft: false,
			        tags: "",
							imglink: imglink,
							opRespId: "",
							shared: true,
							sharedAuthor: data.author,
							sourceUrl: url,
							domain: data.domain,
							loc : {
			            type: "Point",
			            coordinates: [ lng, lat ]
			        },
							lat: lat,
							lng: lng
			    });
			} else {
				console.log("fail");
			}
		}));
	},

	publish: function(title, subtitle, category, subjectType, city, sHTML, draft, tags, imglink, opRespId, articleUsername, hotness, lat, lng){
		Articles.insert({
	        title: title,
	        subtitle: subtitle,
	        category: category,
	        subjectType: subjectType,
	        city: city,
	        sHTML: sHTML,
	        createdAt: new Date(),
					date: (new Date()).getTime(),
	        owner: Meteor.userId(),
	        articleUsername: articleUsername,
	        likes: 0,
					dislikes: 0,
	        citations: 0,
	        flags: 0,
					hotness: hotness,
	        users: [],
	        draft: draft,
	        tags: tags,
					imglink: imglink,
					opRespId: opRespId,
					shared: false,
					sourceUrl: "",
					loc : {
	            type: "Point",
	            coordinates: [ lng, lat ]
	        },
					lat: lat,
					lng: lng
	    });
	},

	userSubscribe: function(owner){
		if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner}).count() > 0) {
			Meteor.users.update(
				{_id: Meteor.userId()},

				{$pull:
					{"profile.users":
						owner
					}
				}
			);
		}
		else {
			Meteor.users.update(
				{_id: Meteor.userId()},

				{$push:
					{"profile.users":
							owner
					}
				}
			);
		};
	},

	updateProfile: function(firstname, lastname, twitter, location, email, quote, website, bio){
		Meteor.users.update(
			{_id: Meteor.userId()},

			{$set:
				{
					"profile.name": firstname,
					"profile.lastname": lastname,
					"profile.twitter": twitter,
					"profile.location": location,
					"profile.email": email,
					"profile.quote": quote,
					"profile.website": website,
					"profile.bio": bio
				}
			}
		);
	},

	hotness: function(currentArticleId, hotness){
		Articles.update({_id: currentArticleId}, {$set: {hotness: hotness}});
	},

	flag: function(currentArticleId){
		if (Articles.find({_id: currentArticleId, usersFlag: Meteor.userId()}).count() > 0) {
			//nothing
		}
		else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
			Articles.update({_id: currentArticleId}, {$push:{usersFlag: Meteor.userId()}, $inc: {flags: 1}});
		};
	}
	/*sendMessage : function(name, message, room, state){
		Messages.insert({name: name, user: Meteor.user().username, msg: message, ts: new Date(), room: room, state: state});
	}*/
});
