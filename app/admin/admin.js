if (Meteor.isClient) {
  Template.admin.helpers({
    numUsers: function() {
      return Meteor.users.find({}).count();
    },
    numUsersTwitter: function(){
      return Meteor.users.find({"services.twitter": {$exists: true}}).count();
    },
    numUsersFB: function(){
      return Meteor.users.find({"services.facebook": {$exists: true}}).count();
    },
    numArticles: function(){
      return Articles.find({}).count();
    }
  });
}
