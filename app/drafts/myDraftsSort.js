if (Meteor.isClient) {

  /*Sort of mySubs*/
  Template.mydrafts.created = function() {
    Session.set('limit', 15);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.mydrafts.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.mydrafts.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.mydrafts.helpers({
    articles: function(){
        return Articles.find({owner: Meteor.userId(), draft: true}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });
}
