if (Meteor.isClient) {

  /*Sort of mySubs*/
  Template.mysubs.created = function() {
    Session.set('limit', 15);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.mysubs.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.mysubs.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.mysubs.helpers({
    articles: function(){
        return Articles.find({owner: { $in: Meteor.user().profile.users}, draft: false}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });
}
