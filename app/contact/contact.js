if (Meteor.isClient) {
    Template.contact.events({
      'submit #contactForm':function(e,t){
        e.preventDefault();
        var name=document.getElementById('contactForm').elements[0].value;
        var email=document.getElementById('contactForm').elements[1].value;
        var message=document.getElementById('contactForm').elements[2].value;

        Meteor.call('sendEmail', name, email, message);
      }
    })
}
