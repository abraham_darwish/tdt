if (Meteor.isClient) {
    Template.constNavFix.helpers({
      search: function(){
        if (Router.current().route.path(this) == '/search') {
          return false;
        }else {
          return true;
        }
      },
      subCount: function(){ //shows the subscribe button only if the user is subscribed to someone
        if (Meteor.user().profile.users.length > 0) {
          return true;
        }else {
          return false;
        }
      }
    });

    Template.constNavStatic.helpers({
      subCount: function(){ //shows the subscribe button only if the user is subscribed to someone
        if (Meteor.user().profile.users.length > 0) {
          return true;
        }else {
          return false;
        }
      }
    });
}
