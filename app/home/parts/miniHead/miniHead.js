if (Meteor.isClient) {
    Template.miniHead.helpers({
      justText: function(subtitle){
        function extractContent(s) {
          var span= document.createElement('span');
          span.innerHTML= s;
          return span.textContent || span.innerText;
        };
        var str = extractContent(subtitle);
        return str;
      },
      encodedTitle: function(title){
        return encodeURIComponent(title);
      }
    });
}
