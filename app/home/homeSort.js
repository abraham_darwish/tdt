/*Home Renda Sort*/
if (Meteor.isClient) {

  Template.homeRenda.created = function() {
    Session.set('limit', 12);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.homeRenda.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.homeRenda.events({
    'click #loadMore':function(evt){
      incrementLimit();
    },
    'click #submit':function(event){
      event.preventDefault();
      var url = document.getElementById('shareUrl').value;
      console.log(url);
      extractMeta(url, function (err, res) {
        if (err) {
          console.log("Error occured " + err);
        } else {
          var category = document.getElementById('category').value;
          var subjectType = document.getElementById('subject').value;
          var imglink = res.image;
          var hotness = Math.round(((((new Date()).getTime()/1000) - 1134028003) / 45000)*10000000)/10000000;
          var lng = Session.get("device-lng");
          var lat = Session.get("device-lat");
          if (subjectType == "local") {
            if (!lng) {
              alert("Must share location to share local article");
              return false;
            }
          };
          if (Meteor.user().services.twitter) {
            var articleUsername = '@'+Meteor.user().services.twitter.screenName;
          } else if (Meteor.user().services.facebook) {
            var articleUsername = '$'+Meteor.user().services.facebook.email;
          } else {
            var articleUsername = Meteor.user().username;
          }
          Meteor.call("publishShared", category, subjectType, imglink, articleUsername, hotness, url, lat, lng);

          if (Meteor.user().services.twitter) {
            Router.go('/j/@' + Meteor.user().services.twitter.screenName);
          } else if (Meteor.user().services.facebook) {
            Router.go('/j/$' + Meteor.user().services.facebook.email);
          } else {
            Router.go('/j/' + Meteor.user().username);
          }
        }
      });
    }
  });

  Template.homeRenda.helpers({
    lat: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lat = startPos.coords.latitude;
        Session.set("device-lat", lat);
        //document.getElementById('startLon').innerHTML = startPos.coords.longitude;
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
      /*setTimeout(function(){
        // check for Geolocation support
        if (navigator.geolocation) {
          console.log('Geolocation is supported!');
          var currentLocation = Geolocation.latLng(); // location of post
          var lat = currentLocation.lat;
          Session.set("device-lat", lat);
        }
        else {
          console.log('Geolocation is not supported for this Browser/OS version yet.');
        }
      }, 3000);*/
    },
    lng: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lng = startPos.coords.longitude;
        Session.set("device-lng", lng);
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    },
    articles: function(){
      return Articles.find({draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });
}
