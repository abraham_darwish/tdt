/*Home Renda Sort*/
if (Meteor.isClient) {

  Template.home.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.home.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }

  Template.home.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });

  Template.home.helpers({
    articles: function(){
      return Articles.find({draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    },
    articlesMini: function(){
      return Articles.find({draft: false, shared: false}, {sort: {hotness: -1}, limit: 4});
    },
    articleTiny: function(){
      return Articles.find({draft: false}, {sort: {createdAt: -1}, limit: 6});
    },
  });
}
