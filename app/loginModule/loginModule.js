if (Meteor.isClient) {
  Template._loginButtonsAdditionalLoggedInDropdownActions.helpers({
    draftsExist: function(){
      if (Articles.find({owner: Meteor.userId(), draft: true}).count() > 0) {
        return true;
      }else {
        return false;
      }
    }
  });

  Template._loginButtonsLoggedInDropdown.events({
    'click #admin': function(event) {
        Router.go('/admin');
    },
    'click #mydrafts': function(event) {
        Router.go('/mydrafts');
    },
    'click #login-buttons-edit-profile': function(event) {
        Router.go('/editProfile');
    },
    'click #myPage':function(event){
      if (Meteor.user().services.twitter) {
        Router.go('/j/@' + Meteor.user().services.twitter.screenName);
      } else if (Meteor.user().services.facebook) {
        Router.go('/j/$' + Meteor.user().services.facebook.email);
      } else {
        Router.go('/j/' + Meteor.user().username);
      }
    },
    'click #tips':function(event){
      Router.go('/tips/');
    }
  });

  Accounts.ui.config({
    forceUsernameLowercase: true,
    passwordSignupFields: "USERNAME_AND_EMAIL",
    extraSignupFields: [{
        fieldName: 'name',
          fieldLabel: 'First name',
          inputType: 'text',
          visible: true,
          validate: function(value, errorFunction) {
            if (!value) {
              errorFunction("Please write your first name");
              return false;
            } else {
              return true;
            }
          }
        }, {
        fieldName: 'lastname',
            fieldLabel: 'Last name',
            inputType: 'text',
            visible: true,
            validate: function(value, errorFunction) {
            if (!value) {
              errorFunction("Please write your last name");
              return false;
            } else {
              return true;
            }
          }
        },{
        fieldName: 'terms',
          fieldLabel: function(){
              return "I accept the terms and conditions".fontcolor("orange");
          },
          inputType: 'checkbox',
          visible: true,
          validate: function(value, errorFucntion){
            if (value) {
              return true
            } else {
              errorFucntion('You must accept the terms and conditions.');
              return false;
            }
          }
        }]
  });
}
