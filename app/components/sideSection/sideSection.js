if (Meteor.isClient) {
  Template.sideSection.helpers({
    author: function(owner) {
      var user = Meteor.users.find({_id: owner});
      return user.map(function (person) {
        if (person.services.twitter) {
          return person.profile.name;
        } else if (person.services.facebook) {
          return person.profile.name;
        } else {
          return person.profile.name + ' ' + person.profile.lastname;
        }
      });
    },
    justText: function(subtitle){
      function extractContent(s) {
        var span= document.createElement('span');
        span.innerHTML= s;
        return span.textContent || span.innerText;
      };
      var str = extractContent(subtitle);
      return str;
    }
  });
}
