if (Meteor.isClient) {
  Template.trendSideSection.helpers({
    articles: function(){
      return Articles.find({draft: false}, {sort: {hotness: -1}, limit:6});
    }
  });
}
