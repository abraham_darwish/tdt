if (Meteor.isClient) {
  Template.articleHead.helpers({
    author: function(owner) {
      var user = Meteor.users.find({_id: owner});
      return user.map(function (person) {
        if (person.services.twitter) {
          return person.profile.name;
        } else if (person.services.facebook) {
          return person.profile.name;
        } else {
          return person.profile.name + ' ' + person.profile.lastname;
        }
      });
    },
    categoryCheck: function(category){
      if (category == "news") {
        return true;
      } else {
        return false;
      }
    },
    someText: function(sHTML){
      function extractContent(s) {
        var span= document.createElement('span');
        span.innerHTML= s;
        return span.textContent || span.innerText;
      };
      var str = extractContent(sHTML);
      if (str.length > 140) {
          return str.substring(0, 140) + '...';
      } else {
        return str.substring(0, str.length);
      }
    },
    justText: function(subtitle){
      function extractContent(s) {
        var span= document.createElement('span');
        span.innerHTML= s;
        return span.textContent || span.innerText;
      };
      var str = extractContent(subtitle);
      return str;
    },
    twitUser: function(owner) {
      var user = Meteor.users.find({_id: owner});
      return user.map(function (person) {
        if (person.services.twitter) {
          return '@'+person.services.twitter.screenName;
        } else {
          return person.profile.twitter;
        }
      });
    }
  });

  Template.articleHead.events({
    'click #reply':function(event, template){
      window.newsId = this._id;
      Router.go('/postform');
    }
  });
}
