/*Life Sort*/
if (Meteor.isClient) {

  /*Sort of viral!*/
  /*Sort of lifeNewsOp*/
  Template.lifeNewsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "life");
    });
  }
  Template.lifeNewsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.lifeNewsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.lifeNewsOp.helpers({
    articles: function(){
      return Articles.find({subjectType: "life", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of lifeNews*/
  Template.lifeNews.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "life");
    });
  }
  Template.lifeNews.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.lifeNews.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.lifeNews.helpers({
    articles: function(){
      return Articles.find({category: "news", subjectType: "life", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of lifeOp*/
  Template.lifeOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "life");
    });
  }
  Template.lifeOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.lifeOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.lifeOp.helpers({
    articles: function(){
      return Articles.find({category: "opinion", subjectType: "life", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });
}
