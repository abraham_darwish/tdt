/*US News Sort*/
if (Meteor.isClient) {
  /*Sort of viral!*/
  Template.politicsNewsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "politics");
    });
  }
  Template.politicsNewsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.politicsNewsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.politicsNewsOp.helpers({
    articles: function(){
      return Articles.find({subjectType: "politics", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  Template.politicsNews.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "politics");
    });
  }
  Template.politicsNews.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.politicsNews.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.politicsNews.helpers({
    articles: function(){
      //var twoDay = new Date((new Date).setDate(new Date().getDate() - 2)); //1 is the number of days
      return Articles.find({category: "news", subjectType: "politics", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  Template.politicsOpinion.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "politics");
    });
  }
  Template.politicsOpinion.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.politicsOpinion.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.politicsOpinion.helpers({
    articles: function(){
      return Articles.find({category: "opinion", subjectType: "politics", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });
}
