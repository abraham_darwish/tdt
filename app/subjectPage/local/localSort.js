/*local Sort*/
if (Meteor.isClient) {
  /*Sort of viral!*/

  Template.local.helpers({
    lat: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lat = startPos.coords.latitude;
        Session.set("device-lat", lat);
        //document.getElementById('startLon').innerHTML = startPos.coords.longitude;
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
      /*setTimeout(function(){
        // check for Geolocation support
        if (navigator.geolocation) {
          console.log('Geolocation is supported!');
          var currentLocation = Geolocation.latLng(); // location of post
          var lat = currentLocation.lat;
          Session.set("device-lat", lat);
        }
        else {
          console.log('Geolocation is not supported for this Browser/OS version yet.');
        }
      }, 3000);*/
    },
    lng: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lng = startPos.coords.longitude;
        Session.set("device-lng", lng);
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    },
    city: function() {
      var lng = Session.get("device-lng");
      var lat = Session.get("device-lat");
      reverseGeocode.getLocation(lat, lng, function(location) {
        var city = reverseGeocode.getAddrStr().split(',')[1]; //gets city only
        Session.set('city', city);
      });
      var currentCity = Session.get("city");
      return currentCity;
    }
  });

  /*Sort of localNewsOp*/
  Template.localNewsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "local");
    });
  }
  Template.localNewsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.localNewsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.localNewsOp.helpers({
    articles: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         draft: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000 //40 km
              }
           }
       }, {sort: {hotness: -1}, limit: Session.get('limit')});
      if (results.count() <= 0) {
        return false;
      } else {
        return results;
      }
    }
  });

  /*Sort of localNews*/
  Template.localNews.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "local");
    });
  }
  Template.localNews.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.localNews.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.localNews.helpers({
    articles: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         category: "news",
         draft: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000 //40 km
              }
           }
       }, {sort: {hotness: -1}, limit: Session.get('limit')});
       if (results.count() <= 0) {
         return false;
       } else {
         return results;
       }
    }
  });

  /*Sort of localOp*/
  Template.localOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "local");
    });
  }
  Template.localOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.localOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.localOp.helpers({
    articles: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         category: "opinion",
         draft: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000
              }
           }
       }, {sort: {hotness: -1}, limit: Session.get('limit')});
       if (results.count() <= 0) {
         return false;
       } else {
         return results;
       }
    }
  });

  /*Sort of AmericasMonth
  Template.umdMonth.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.umdMonth.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.umdMonth.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.umdMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30));
      return Articles.find({subjectType: "umd", draft: false, createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });


  Template.umdYear.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.umdYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.umdYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.umdYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365));
      return Articles.find({category: "world", draft: false, wldcategory: "americas", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });


  Template.umdAll.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.umdAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.umdAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.umdAll.helpers({
    articles: function(){
      return Articles.find({subjectType: "umd", draft: false}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });*/
}
