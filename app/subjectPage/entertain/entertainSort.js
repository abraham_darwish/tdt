/*Entertainment Sort*/
if (Meteor.isClient) {
  /*Sort of entertainNews*/
  Template.entertainNews.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "entertain");
    });
  }
  Template.entertainNews.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.entertainNews.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.entertainNews.helpers({
    articles: function(){
      return Articles.find({category: "news", subjectType: "entertain", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of entertainOp*/
  Template.entertainOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "entertain");
    });
  }
  Template.entertainOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.entertainOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.entertainOp.helpers({
    articles: function(){
      return Articles.find({category: "opinion", subjectType: "entertain", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of entertainNewsOp*/
  Template.entertainNewsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "entertain");
    });
  }
  Template.entertainNewsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.entertainNewsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.entertainNewsOp.helpers({
    articles: function(){
      return Articles.find({subjectType: "entertain", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

}
