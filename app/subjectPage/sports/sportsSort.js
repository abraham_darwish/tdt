/*sports Sort*/
if (Meteor.isClient) {
/*
  Template.trendSideSection.helpers({
    articles: function(){
      return Articles.find({subjectType: "umd", draft: false}, {limit:6});
    }
  });*/
  /*Sort of viral!*/
  /*Sort of sportsNewsOp*/
  Template.sportsNewsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "sports");
    });
  }
  Template.sportsNewsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sportsNewsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sportsNewsOp.helpers({
    articles: function(){
      return Articles.find({subjectType: "sports", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of sportsNews*/
  Template.sportsNews.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "sports");
    });
  }
  Template.sportsNews.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sportsNews.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sportsNews.helpers({
    articles: function(){
      return Articles.find({category: "news", subjectType: "sports", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of sportsOp*/
  Template.sportsOp.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesSubject', Session.get('limit'), "sports");
    });
  }
  Template.sportsOp.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sportsOp.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sportsOp.helpers({
    articles: function(){
      return Articles.find({category: "opinion", subjectType: "sports", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    }
  });
}
