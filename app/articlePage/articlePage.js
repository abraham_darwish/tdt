if (Meteor.isClient) {
	Template.articlePage.helpers({
    thisPage: function(){
      return Router.current().route.path(this);
    },
    author: function(owner) {
      var user = Meteor.users.find({_id: owner});
			return user.map(function (person) {
				if (person.services.twitter) {
					return person.profile.name;
				} else if (person.services.facebook) {
					return person.profile.name;
				} else {
					return person.profile.name + ' ' + person.profile.lastname;
				}
			});
    },
		citationCheck: function(citations){ //check if the number of citations >99 and if they are then put them next to the like button
			if (citations > 99) {
				return true;
			} else {
				return false;
			}
		},
		categoryCheck: function(category){
      if (category == "news") {
        return true;
      } else {
        return false;
      }
    },
    delete: function(owner){
      if (Meteor.userId() == owner || Meteor.user().admin == true) {
        return true;
      }
      else {
        return false;
      }
    },
		sideExists: function(owner, _id){
			if ((Articles.find({opRespId: _id, draft: false}).count() > 0) && (Articles.find({owner: owner, draft: false, _id: {$not: _id}}).count() > 0)) { //if there are trending articles and articles by the author
				return true;
			}
		},
		imgExists: function(imglink){
			function myCallback(url, answer) {
				if (answer) {
					$(document).ready(function() {
						 $('header').css('background-image', 'url('+url+')');
				 });
				}
				else {
						$(document).ready(function() {
							 $('header').css('background-image', 'url('+randImg()+')');
					 });
				}
			}

			function IsValidImageUrl(url, callback) {
			    var img = new Image();
			    img.onerror = function() { callback(url, false);}
			    img.onload =  function() { callback(url, true);}
			    img.src = url
			}

			function randImg(){
				var d = new Date();
				var num = d.getDay();
				if (num == 1) { //monday
					return '/img/portals/images/19.jpg';
				} else if (num == 2) {
					return '/img/portals/images/22.jpg';
				} else if (num == 3) {
					return '/img/portals/images/54.jpeg';
				} else if (num == 4) {
					return '/img/13.jpeg';
				} else if (num == 5) {
					return '/img/portals/images/20.jpg';
				}else if (num == 6) {
					return '/img/portals/images/47.jpg';
				} else {
					return '/img/portals/images/51.jpg';
				}
			}
			IsValidImageUrl(imglink, myCallback);
		},
		twitCheck: function(owner) {
      var user = Meteor.users.find({_id: owner});
			return user.map(function (person) {
        if (person.services.twitter) {
          return '@'+person.services.twitter.screenName;
        }else {
          return person.profile.twitter;
        }
			});
    },
		heartDis: function(currentArticleId){
			var delay = 100; /*.1 second*/
			setTimeout(function(){
					if (Articles.find({_id: currentArticleId, usersDis: Meteor.userId()}).count() > 0) {return document.getElementById("dislike").style.color = "green"} /*If previously liked*/
					else{return document.getElementById("dislike").style.color = "orange"};
			}, delay);
		},
		heartLike: function(currentArticleId){
			var delay = 100; /*.1 second*/
			setTimeout(function(){
					if (Articles.find({_id: currentArticleId, users: Meteor.userId()}).count() > 0) {return document.getElementById("like").style.color = "red"} /*If previously liked*/
					else{return document.getElementById("like").style.color = "orange"};
			}, delay);
		},
		articlesOp: function(currentArticleId){
      return Articles.find({opRespId: currentArticleId, draft: false}, {sort: {hotness: -1}, limit: 6});
    },
		articlesTrend: function(){
			return Articles.find({draft: false}, {sort: {hotness: -1}, limit: 6});
		},
    myArticles: function(currentArticleId){
      var article = Articles.find({_id: currentArticleId});
      var owner = article.map(function (person) {
        return person.owner;
      });
      return Articles.find({owner: owner[0], draft: false, _id: {$not: currentArticleId}}, {sort: {createdAt: -1}, limit: 6});
    },
		authorOtherArticles: function(owner){
			if (Articles.find({owner: owner, draft: false}).count() > 1) {
				return true;
			}else {
				return false;
			}
		},
		justText: function(subtitle){
      function extractContent(s) {
        var span= document.createElement('span');
        span.innerHTML= s;
        return span.textContent || span.innerText;
      };
      var str = extractContent(subtitle);
      return str;
    }
  });

	Template.articlePage.events({
		'click #reply':function(event, template){
			event.preventDefault();
			window.newsId = this._id;
      Router.go('/postform');
    },
		'click .like':function(event){
			/*Prevent Form from submitting*/
			event.preventDefault();
			var path = window.location.pathname;//Router.current().route.path(this);
			var thisPath = path.split('/');
			currentArticleId = thisPath[2];
			/*Make sure people only like once*/
			Meteor.call("likePage", currentArticleId);
			/* Hotness */
			function hot (likes, dislikes, date){
				var score = likes - dislikes;
				var order = Math.log(Math.max(Math.abs(score), 1)) / Math.LN10;
				var sign = score > 0 ? 1 : score < 0 ? -1 : 0;
				var seconds = (date/1000) - 1134028003;
				var product = order + sign * seconds / 45000;
				return Math.round(product*10000000)/10000000;
			};
			setTimeout(function(){
				var art = Articles.find({_id: currentArticleId});
				var likes = art.map(function (piece) {
							return piece.likes;
				});
				var dislikes = art.map(function (piece) {
						return piece.dislikes;
				});
				var date = art.map(function (piece) {
						return piece.date;
				});
				var hotness = hot(likes, dislikes, date);
				Meteor.call("hotness", currentArticleId, hotness);
			}, 10000); //10 seconds
			//change colors
			if (Articles.find({_id: currentArticleId, users: Meteor.userId()}).count() > 0) {
				return document.getElementById("like").style.color = "orange"; /*Updates the color as soon as it changes*/
			}
			else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
				document.getElementById("dislike").style.color = "orange";
				return document.getElementById("like").style.color = "red";
			};
			return false;
		},
		'click .dislike':function(event){
			/*Prevent Form from submitting*/
			event.preventDefault();
			var path = window.location.pathname;//Router.current().route.path(this);
			var thisPath = path.split('/');
			currentArticleId = thisPath[2];
			/*Make sure people only like once*/
			Meteor.call("dislikePage", currentArticleId);
			/* Hotness */
			function hot (likes, dislikes, date){
				var score = likes - dislikes;
				var order = Math.log(Math.max(Math.abs(score), 1)) / Math.LN10;
				var sign = score > 0 ? 1 : score < 0 ? -1 : 0;
				var seconds = (date/1000) - 1134028003;
				var product = order + sign * seconds / 45000;
				return Math.round(product*10000000)/10000000;
			};
			setTimeout(function(){
				var art = Articles.find({_id: currentArticleId});
				var likes = art.map(function (piece) {
							return piece.likes;
				});
				var dislikes = art.map(function (piece) {
						return piece.dislikes;
				});
				var date = art.map(function (piece) {
						return piece.date;
				});
				var hotness = hot(likes, dislikes, date);
				Meteor.call("hotness", currentArticleId, hotness);
			}, 10000); //10 seconds
			//change colors
			if (Articles.find({_id: currentArticleId, usersDis: Meteor.userId()}).count() > 0) {
				return document.getElementById("dislike").style.color = "orange"; /*Updates the color as soon as it changes*/
			}
			else { /*If user didn't like before add user, increment like counter by 1 and say that user liked the post*/
				document.getElementById("like").style.color = "orange";
				return document.getElementById("dislike").style.color = "green";
			};
			return false;
		},

		'click .cite':function(event){
			/*Prevent Form from submitting*/
			event.preventDefault();
			//var currentArticleId = window.location.href.split('/').pop(); /*Get the id of the article*/
			var path = Router.current().route.path(this);
			var thisPath = path.split('/');
			currentArticleId = thisPath[2];
			/*Make sure people only like once*/
			Meteor.call("userCite", currentArticleId); //userCite MUST be before citePage because citePage adds that the user cited the article and userCite checks that and if true it wont do what its supposed to
			Meteor.call("citePage", currentArticleId);
		},
		'click .deleteArticle':function(event){
			/*Prevent Form from submitting*/
			event.preventDefault();
			var refresh = confirm("Are you sure you would like to OK deleting the article? It will be lost forever");
      if (refresh==true) {
				var path = Router.current().route.path(this);
				var thisPath = path.split('/');
				currentArticleId = thisPath[2];
				Meteor.call("deleteArticle", currentArticleId);
				Router.go('home');
			}
		},
		'click #flag':function(event, template){
			/*Prevent Form from submitting*/
			event.preventDefault();
			var currentArticleId = this._id;
			Meteor.call("flag", currentArticleId);
			alert("Article has been flagged and will be reviewed. If you have remaining concerns email info@tdtimes.net");
		}
	});
}
