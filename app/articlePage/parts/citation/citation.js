if (Meteor.isClient) {
  Template.citation.helpers({
      thisPage: function(){
        return Router.current().route.path(this);
      },
      name: function(owner) {
        var user = Meteor.users.find({_id: owner});
  			return user.map(function (person) {
          if (person.services.twitter) {
            if (person.profile.lastname == "") {
              var lastname = person.profile.name.split(' ')[1];
              var firstname = person.profile.name.split(' ')[0];
              return lastname + ', ' + firstname;
            } else {
              return person.profile.lastname + ', ' + person.profile.name;
            }
          } else if (person.services.facebook) {
              return person.services.facebook.last_name + ',' + ' ' + person.services.facebook.first_name;
          } else {
              return person.profile.lastname + ', ' + person.profile.name;
          }
  			});
      }
  });
}
