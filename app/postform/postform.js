if (Meteor.isClient) {
  Template.postform.helpers({
    lat: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lat = startPos.coords.latitude;
        Session.set("device-lat", lat);
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    },
    lng: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lng = startPos.coords.longitude;
        Session.set("device-lng", lng);
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    },
  });

  Template.postform.events({
    /*Both preview buttons scrolls down to draft*/
    'click .previewdraft, #preview2':function(event){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $("#draft").offset().top
      }, 2500);
      return false;
    },

    //Discards entries into form as in go to home
    'click .discard':function(event){
      event.preventDefault();
      var refresh = confirm("Are you sure you would like to OK discarding article? It will be lost");
      if (refresh==true) { Router.go('home'); }
      return false;
    },

    //Open new page/tab with sample post
    'click .sample':function(event){
      event.preventDefault();
      window.open(Router.url("post")) //opens the sample post in a new window
      return false;
    },
    //Open new page/tab with Chartbuilder
    'click .chart':function(event){
      event.preventDefault();
      window.open("http://app.raw.densitydesign.org"); //opens the Chartbuilder in a new window
      return false;
    },
    //Scolls up to form
    'click .fixup, #fixup2':function(event){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $("#post").offset().top
      }, 2500);
      return false;
    },

    //adds form inputs to db with draft: true and redirects to dash
    'click .savedraft, #savedraft2':function(event){
      event.preventDefault();
      var savedraft = confirm("This will redirect you to your drafts page");
      if (savedraft==true) {
        console.log("hello savedraft");
        var title = document.getElementById('post').elements[4].value;
        var subtitle = document.getElementById('post').elements[5].value;
        var tags = document.getElementById('post').elements[6].value;
        var imglink = document.getElementById('post').elements[7].value;
        var category = document.getElementById('post').elements[8].value;
        var subjectType = document.getElementById('post').elements[9].value;
        var city = document.getElementById('post').elements[10].value;
        var sHTML = $('#summernote').summernote('code');
        var opRespId = window.newsId; //when responding Opinion Respond Id
        var draft = true;
        var hotness = 1;
        var shared = false;
        var sourceUrl = "";
        var lat = "";
        var lng = "";
        if (Meteor.user().services.twitter) {
          var articleUsername = '@'+Meteor.user().services.twitter.screenName;
        } else if (Meteor.user().services.facebook) {
          var articleUsername = '$'+Meteor.user().services.facebook.email;
        } else {
          var articleUsername = Meteor.user().username;
        }
        Meteor.call("publish", title, subtitle, category, subjectType, city, sHTML, draft, tags, imglink, opRespId, articleUsername, hotness, shared, sourceUrl, lat, lng);
        window.newsId = "";
        Router.go('/mydrafts');
      };
    },

    //publish add to db and redirect to user page
    'click .publish, #publish2':function(event){
      event.preventDefault();
      var title = document.getElementById('post').elements[4].value;
      var subtitle = document.getElementById('post').elements[5].value;
      var tags = document.getElementById('post').elements[6].value;
      var imglink = document.getElementById('post').elements[7].value;
      var category = document.getElementById('post').elements[8].value;
      var subjectType = document.getElementById('post').elements[9].value;
      var city = document.getElementById('post').elements[10].value;
      var sHTML = $('#summernote').summernote('code');
      var opRespId = window.newsId; //when responding Opinion Respond Id
      var draft = false;
      var hotness = Math.round(((((new Date()).getTime()/1000) - 1134028003) / 45000)*10000000)/10000000;
      var lng = Session.get("device-lng");
      var lat = Session.get("device-lat");
      if (Meteor.user().services.twitter) {
        var articleUsername = '@'+Meteor.user().services.twitter.screenName;
      } else if (Meteor.user().services.facebook) {
        var articleUsername = '$'+Meteor.user().services.facebook.email;
      } else {
        var articleUsername = Meteor.user().username;
      }
      Meteor.call("publish", title, subtitle, category, subjectType, city, sHTML, draft, tags, imglink, opRespId, articleUsername, hotness, lat, lng);
      window.newsId = "";
      if (Meteor.user().services.twitter) {
        Router.go('/j/@' + Meteor.user().services.twitter.screenName);
      } else if (Meteor.user().services.facebook) {
        Router.go('/j/$' + Meteor.user().services.facebook.email);
      } else {
        Router.go('/j/' + Meteor.user().username);
      }
    }
  });
}
