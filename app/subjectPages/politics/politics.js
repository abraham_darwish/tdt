/*politics Sort*/
if (Meteor.isClient) {

  Template.politics.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.politics.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }

  Template.politics.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });

  Template.politics.helpers({
    articles: function(){
      return Articles.find({subjectType: "politics", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    },
    articlesMini: function(){
      return Articles.find({subjectType: "politics", draft: false, shared: false}, {sort: {hotness: -1}, limit: 4});
    },
    articleTiny: function(){
      return Articles.find({subjectType: "politics", draft: false}, {sort: {createdAt: -1}, limit: 6});
    },
  });
}
