/*local Sort*/
if (Meteor.isClient) {

  Template.local.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.local.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }

  Template.local.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });

  Template.local.helpers({
    lat: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lat = startPos.coords.latitude;
        Session.set("device-lat", lat);
        //document.getElementById('startLon').innerHTML = startPos.coords.longitude;
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
      /*setTimeout(function(){
        // check for Geolocation support
        if (navigator.geolocation) {
          console.log('Geolocation is supported!');
          var currentLocation = Geolocation.latLng(); // location of post
          var lat = currentLocation.lat;
          Session.set("device-lat", lat);
        }
        else {
          console.log('Geolocation is not supported for this Browser/OS version yet.');
        }
      }, 3000);*/
    },
    lng: function() {
      var startPos;
      var geoOptions = {
         timeout: 10 * 1000
      }
      var geoSuccess = function(position) {
        startPos = position;
        var lng = startPos.coords.longitude;
        Session.set("device-lng", lng);
      };
      var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    },
    city: function() {
      var lng = Session.get("device-lng");
      var lat = Session.get("device-lat");
      reverseGeocode.getLocation(lat, lng, function(location) {
        var city = reverseGeocode.getAddrStr().split(',')[1]; //gets city only
        Session.set('city', city);
      });
      var currentCity = Session.get("city");
      return currentCity;
    },
    articles: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         draft: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000 //40 km
              }
           }
       }, {sort: {hotness: -1}, limit: Session.get('limit')});
      if (results.count() <= 0) {
        return false;
      } else {
        return results;
      }
    },
    articlesMini: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         draft: false,
         shared: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000 //40 km
              }
           }
       }, {sort: {hotness: -1}, limit: 4});
      if (results.count() <= 0) {
        return false;
      } else {
        return results;
      }
    },
    articleTiny: function(){
        var lng = Session.get("device-lng");
        var lat = Session.get("device-lat");
        var results = Articles.find( {
         draft: false,
         loc:
           { $near :
              {
                $geometry:{ type:"Point", coordinates:[ lng, lat]},
                $minDistance: 0,
                $maxDistance: 40000 //40 km
              }
           }
       }, {sort: {createdAt: -1}, limit: 6});
      if (results.count() <= 0) {
        return false;
      } else {
        return results;
      }
    }
  });
}
