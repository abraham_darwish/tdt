/*entertain Sort*/
if (Meteor.isClient) {

  Template.entertain.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.entertain.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }

  Template.entertain.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });

  Template.entertain.helpers({
    articles: function(){
      return Articles.find({subjectType: "entertain", draft: false}, {sort: {hotness: -1}, limit: Session.get('limit')});
    },
    articlesMini: function(){
      return Articles.find({subjectType: "entertain", draft: false, shared: false}, {sort: {hotness: -1}, limit: 4});
    },
    articleTiny: function(){
      return Articles.find({subjectType: "entertain", draft: false}, {sort: {createdAt: -1}, limit: 6});
    },
  });
}
