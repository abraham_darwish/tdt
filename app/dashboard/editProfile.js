if (Meteor.isClient){
	Template.editProfile.events({
		'submit form': function(event){ //#updateProfileBtn
			event.preventDefault();

			if (! Meteor.user().services.twitter){
				var firstname = document.getElementById('editProfile').elements[0].value;
				var lastname = document.getElementById('editProfile').elements[1].value;
				var twitter = document.getElementById('editProfile').elements[2].value;
				var location = document.getElementById('editProfile').elements[3].value;
	    	var email = document.getElementById('editProfile').elements[4].value;
	    	var quote = document.getElementById('editProfile').elements[5].value;
	    	var website = document.getElementById('editProfile').elements[6].value;
	    	var bio = document.getElementById('editProfile').elements[7].value;
			} else {
				var firstname = document.getElementById('editProfile').elements[0].value;
				var lastname = document.getElementById('editProfile').elements[1].value;
				var twitter = "";
				var location = document.getElementById('editProfile').elements[2].value;
	    	var email = document.getElementById('editProfile').elements[3].value;
	    	var quote = document.getElementById('editProfile').elements[4].value;
	    	var website = document.getElementById('editProfile').elements[5].value;
	    	var bio = document.getElementById('editProfile').elements[6].value;
			};

			Meteor.call("updateProfile", firstname, lastname, twitter, location, email, quote, website, bio);

			/*Clear form*/
			document.getElementById('editProfile').elements[0].value = "";
			document.getElementById('editProfile').elements[1].value = "";
			document.getElementById('editProfile').elements[2].value = "";
			document.getElementById('editProfile').elements[3].value = "";
			document.getElementById('editProfile').elements[4].value = "";
			document.getElementById('editProfile').elements[5].value = "";
			document.getElementById('editProfile').elements[6].value = "";
			if (! Meteor.user().services.twitter){ document.getElementById('editProfile').elements[7].value = ""; };

			if (Meteor.user().services.twitter) {
        	Router.go('/j/@' + Meteor.user().services.twitter.screenName);
      } else if (Meteor.user().services.facebook) {
      	Router.go('/j/$' + Meteor.user().services.facebook.email);
      } else {
				Router.go('/j/' + Meteor.user().username);
      }
		}
	})
}
