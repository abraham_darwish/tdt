if (Meteor.isClient){
	Template.editProfile.events({
		'click #update': function(event){ //#updateProfileBtn
			event.preventDefault();

			if (! Meteor.user().services.twitter){
				var firstname = document.getElementById('firstname').value;
				var lastname  = document.getElementById('lastname').value;
				var twitter   = document.getElementById('twitter').value;
	    	var quote     = document.getElementById('quote').value;
	    	var website   = document.getElementById('website').value;
			} else {
        var firstname = document.getElementById('firstname').value;
				var lastname  = document.getElementById('lastname').value;
				var twitter   = "";
	    	var quote     = document.getElementById('quote').value;
	    	var website   = document.getElementById('website').value;
			};

			Meteor.call("updateProfile", firstname, lastname, twitter, quote, website);

			/*Clear form*/
      document.getElementById('firstname').value = "";
      document.getElementById('lastname').value  = "";
      document.getElementById('quote').value     = "";
      document.getElementById('website').value   = "";
			if (! Meteor.user().services.twitter){ document.getElementById('twitter').value   = ""; };

			if (Meteor.user().services.twitter) {
        	Router.go('/j/@' + Meteor.user().services.twitter.screenName);
      } else if (Meteor.user().services.facebook) {
      	Router.go('/j/$' + Meteor.user().services.facebook.email);
      } else {
				Router.go('/j/' + Meteor.user().username);
      }
		}
	})
}
