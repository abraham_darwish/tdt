if (Meteor.isClient) {

	Template.dashboard.helpers({
    	articles: function(currentProfile){
      		return Articles.find({owner: currentProfile, draft: false}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    	},
				myPageCheck: function(){ //prevent people from subscribing to themselves by not showing them the sub button
					var delay = 100; /*.1 second*/
					setTimeout(function(){
					var path = window.location.pathname; //Router.current().route.path(this) apparently doesnt work all the time. Full URL: Router.current().url
					var thisPath = path.split('/');
					var currentProfile = thisPath[2];
					if (Meteor.user().services.twitter) {
							if (currentProfile == ('@'+Meteor.user().services.twitter.screenName)) { //if logged in with twitter
								return false; //if you're on your page dont see the sub button
							}else {
								return true;
							}
					} else if (Meteor.user().services.facebook) {
						if (currentProfile == ('$'+Meteor.user().services.facebook.email)) { //if logged in with twitter
							return false; //if you're on your page dont see the sub button
						}else {
							return true;
						}
					} else {
							if (currentProfile == Meteor.user().username) {
								return false; //if you're on your page dont see the sub button
							} else {
								return true;
							}
					}
				}, delay);
			},
			userSubColor: function(){
				var delay = 100; /*0.1 second*/
				setTimeout(function(){
						/*var paths = window.location.href.split('/')
						var userSub = paths[2];*/
						var path = window.location.pathname; //Router.current().route.path(this) apparently doesnt work all the time. Full URL: Router.current().url
						var thisPath = path.split('/');
						var currentProfile = thisPath[2];
						if (currentProfile.charAt(0) == '@') { //return articles for twitter profile
								var user = Meteor.users.find({"services.twitter.screenName": currentProfile.split('@')[1]});
								var owner = user.map(function (person) {
										return person._id;
								});
								if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
									document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
								} /*If previously liked*/
								else{
									document.getElementById("userSubscribe").className = "btn btn-info btn-block";
									document.getElementById("userSubscribe").innerHTML = "Subscribe";
								};
						} else if (currentProfile.charAt(0) == '$') {
							var user = Meteor.users.find({"services.facebook.email": currentProfile.split('$')[1]});
							var owner = user.map(function (person) {
									return person._id;
							});
							if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
								document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
							} /*If previously liked*/
							else{
								document.getElementById("userSubscribe").className = "btn btn-info btn-block";
								document.getElementById("userSubscribe").innerHTML = "Subscribe";
							};
						} else {
								var user = Meteor.users.find({username: currentProfile});
								var owner = user.map(function (person) {
										return person._id;
								});
								if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
									document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
								} /*If previously liked*/
								else{
									document.getElementById("userSubscribe").className = "btn btn-info btn-block";
									document.getElementById("userSubscribe").innerHTML = "Subscribe";
								};
						}
				}, delay);
			}
  	});
		/*Sort of dashboard*/
		Template.dashboard.created = function() {
			Session.set('limit', 10);

			Deps.autorun(function(){
				Meteor.subscribe('articlesInfinite', Session.get('limit'));
			});
		}
		Template.dashboard.rendered = function(){
			$(window).scroll(function(){
				if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
			});
		}

  Template.dashboard.events({
    'click #userSubscribe': function(event){
      /*Prevent Form from submitting*/
      event.defaultPrevented;
      /*Get the name of user we're subscribing to*/
			var path = window.location.pathname; //Router.current().route.path(this) apparently doesnt work all the time. Full URL: Router.current().url
			var thisPath = path.split('/');
			var currentProfile = thisPath[2];
			if (currentProfile.charAt(0) == '@') { //return articles for twitter profile
					var user = Meteor.users.find({"services.twitter.screenName": currentProfile.split('@')[1]});
					var owner = user.map(function (person) {
							return person._id;
					});
					Meteor.call("userSubscribe", owner[0]);
					var delay = 100; /*0.1 second*/
					setTimeout(function(){
						if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
							document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
						} /*If previously liked*/
						else{
							document.getElementById("userSubscribe").className = "btn btn-info btn-block";
							document.getElementById("userSubscribe").innerHTML = "Subscribe";
						};
					}, delay);
			} else if (currentProfile.charAt(0) == '$') {
					var user = Meteor.users.find({"services.facebook.email": currentProfile.split('$')[1]});
					var owner = user.map(function (person) {
							return person._id;
					});
					Meteor.call("userSubscribe", owner[0]);
					var delay = 100; /*0.1 second*/
					setTimeout(function(){
						if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
							document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
						} /*If previously liked*/
						else{
							document.getElementById("userSubscribe").className = "btn btn-info btn-block";
							document.getElementById("userSubscribe").innerHTML = "Subscribe";
						};
					}, delay);
			} else {
					var user = Meteor.users.find({username: currentProfile});
					var owner = user.map(function (person) {
							return person._id;
					});
					Meteor.call("userSubscribe", owner[0]);
					var delay = 100; /*0.1 second*/
					setTimeout(function(){
						if (Meteor.users.find({_id: Meteor.userId(), "profile.users": owner[0]}).count() > 0) {document.getElementById("userSubscribe").className = "btn btn-warning btn-block";
							document.getElementById("userSubscribe").innerHTML = "Unsubscribe";
						} /*If previously liked*/
						else{
							document.getElementById("userSubscribe").className = "btn btn-info btn-block";
							document.getElementById("userSubscribe").innerHTML = "Subscribe";
						};
					}, delay);
			}
      //var userSub = window.location.href.split('/').pop();
      return false;
  	},

		'click #loadMore':function(evt){
			incrementLimit();
		}
  });
}
