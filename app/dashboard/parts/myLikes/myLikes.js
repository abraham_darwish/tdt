if (Meteor.isClient) {
/*Returns the articles that the person likes sorted by most recent one created*/
  Template.myLikes.helpers({
    articles: function(){
      var path = window.location.pathname; //Router.current().route.path(this) apparently doesnt work all the time. Full URL: Router.current().url
      var thisPath = path.split('/');
      var currentProfile = thisPath[2];
      if (currentProfile.charAt(0) == '@') { //return articles for twitter profile
        var user = Meteor.users.find({"services.twitter.screenName": currentProfile.split('@')[1]});
        var owner = user.map(function (person) {
            return person._id;
        });
        return Articles.find({users: owner, draft: false}, {limit:6});
      } else if (currentProfile.charAt(0) == '$') {
        var user = Meteor.users.find({"services.facebook.email": currentProfile.split('$')[1]});
        var owner = user.map(function (person) {
            return person._id;
        });
        return Articles.find({users: owner, draft: false}, {limit:6});
      } else {
        var user = Meteor.users.find({username: currentProfile});
        var owner = user.map(function (person) {
            return person._id;
        });
        return Articles.find({users: owner, draft: false}, {sort: {createdAt: -1}, limit:6});
      }
    }
  });
}
