if (Meteor.isClient) {
	Template.subsList.helpers({
		userPage: function(userId){
			var user = Meteor.users.find({_id: userId});
			return user.map(function (person) {
				if (person.services.twitter) {
					return '@' + person.services.twitter.screenName;
				} else if (person.services.facebook) {
					return '$' + person.services.facebook.email;
				} else {
					return person.username;
				}
			});
		}
	});
}
