if (Meteor.isClient) {

	Template.dashboard.helpers({
    	articles: function(currentProfile){
      		return Articles.find({owner: currentProfile, draft: false}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    	},
      articlesMini: function(currentProfile){
      		return Articles.find({owner: currentProfile, draft: false}, {sort: {createdAt: -1}, limit: 4});
    	},
      articlesTiny: function(){
      		return Articles.find({draft: false}, {sort: {hotness: -1}, limit: 6});
    	}
  	});
		/*Sort of dashboard*/
		Template.dashboard.created = function() {
			Session.set('limit', 5);

			Deps.autorun(function(){
				Meteor.subscribe('articlesInfinite', Session.get('limit'));
			});
		}

  Template.dashboard.events({
		'click #loadMore':function(evt){
			incrementLimit();
		}
  });
}
