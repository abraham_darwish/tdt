if (Meteor.isClient) {

  Template.breakingTwitter.rendered = function(){
    twttr.widgets.load(
      document.getElementById("twit")
    )
    setTimeout(function(){
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }, 0);
  };

  Template.usTwitter.rendered = function(){
    twttr.widgets.load(
      document.getElementById("twit")
    )
    setTimeout(function(){
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }, 0);
  };

  Template.entertainTwitter.rendered = function(){
    twttr.widgets.load(
      document.getElementById("twit")
    )
    setTimeout(function(){
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }, 0);
  };

  Template.lifeTwitter.rendered = function(){
    twttr.widgets.load(
      document.getElementById("twit")
    )
    setTimeout(function(){
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }, 0);
  };

  Template.sportsTwitter.rendered = function(){
    twttr.widgets.load(
      document.getElementById("twit")
    )
    setTimeout(function(){
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }, 0);
  };
}
