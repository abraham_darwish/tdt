/* Routing */
//home

Router.route('/',{
  name: 'home',
  template: 'homeRenda',
  waitOn: function() {
    return Meteor.subscribe('articlesInfinite',12);
  },
  fastRender: true,
  onAfterAction: function() {
    SEO.set({
      title: 'The Digital Times',
      meta: {
        'description'  : 'The homepage of The Digital Times',
        'twitter:card' : "summary_large_image",
        'twitter:site' : "@realtdt",
        'twitter:image': '/img/coverphoto.JPG'
      },
      og: {
        'title'      : 'The Digital Times',
        'description': 'The homepage of The Digital Times',
        'image'      : '/img/coverphoto.JPG'
      }
    });
  }
});
Router.configure({
    loadingTemplate: 'loading',
    notFoundTemplate: "notFound"
});
Router.route('/post');
Router.route('/about', {
  onAfterAction: function() {
    document.title = 'About Page - The Digital Times';
  }
});
Router.route('/upload');
Router.route('/contact');
Router.route('/lists');
Router.route('/lists/:_id', { //page for specific list
  template: 'listPage',
  data: function(){
    var currentList = this.params._id;
    return Lists.findOne({ _id: currentList });
  }
  //console.log(this.params._id);
});
Router.route('/postform', {
  onAfterAction: function() {
    document.title = 'Publish Article - The Digital Times';
  }
});

/*Router.route('/login/');
Router.route('/signup/');


Router.route('/world/');
Router.route('/world/new/',{
  name: 'newWorld',
  path: 'world/new/',
  template: 'newWorld'
});
Router.route('/world/viral/',{
  name: 'viralWorld',
  path: 'world/viral/',
  template: 'viralWorld'
});
Router.route('/world/africa/',{
  name: 'AfricaWorld',
  path: 'world/africa/',
  template: 'AfricaWorld'
});

Router.route('/world/americas/',{
  name: 'AmericasWorld',
  path: 'world/americas/',
  template: 'AmericasWorld'
});

Router.route('/world/asia/',{
  name: 'AsiaWorld',
  path: 'world/asia/',
  template: 'AsiaWorld'
});

Router.route('/world/europe/',{
  name: 'EuropeWorld',
  path: 'world/europe/',
  template: 'EuropeWorld'
});

Router.route('/world/meast/',{
  name: 'meastWorld',
  path: 'world/meast/',
  template: 'meastWorld'
});

Router.route('/world/oceania/',{
  name: 'OceaniaWorld',
  path: 'world/oceania/',
  template: 'OceaniaWorld'
});


Router.route('/world/:_id/',{
    template: 'articlePage',
    data: function(){
      console.log(this.params);
      var currentArticle = this.params._id;
      return Articles.findOne({_id: currentArticle})
    }
});*/


Router.route('/j/@:twitterUsername',{
  template: 'dashboard',
  waitOn: function() {
    return Meteor.subscribe('articlesUser', this.params.twitterUsername, "twitter") && Meteor.subscribe('userPage', this.params.twitterUsername, "twitter");
  },
  fastRender: true,
  data: function(){
    var currentProfile = this.params.twitterUsername;
    var thisUser = Meteor.users.findOne({"services.twitter.screenName": currentProfile});
    if (!thisUser) {
      this.render("notFound");
    } else {
      return thisUser;
    }
  },
  onAfterAction: function() {
    var user = Meteor.users.find({"services.twitter.screenName": this.params.twitterUsername});
    var name = user.map(function (person) {
        return person.profile.name;
    });
    document.title = 'Journalist ' + name + ' - The Digital Times';
  }
});

Router.route('/j/$:fbUsername',{
  template: 'dashboard',
  waitOn: function() {
    return Meteor.subscribe('articlesUser', this.params.fbUsername, "facebook") && Meteor.subscribe('userPage', this.params.twitterUsername, "facebook");
  },
  fastRender: true,
  data: function(){
    var currentProfile = this.params.fbUsername;
    var thisUser = Meteor.users.findOne({"services.facebook.email": currentProfile});
    if (!thisUser) {
      this.render("notFound");
    } else {
      return thisUser;
    }
  },
  onAfterAction: function() {
    var user = Meteor.users.find({"services.facebook.email": this.params.fbUsername});
    var name = user.map(function (person) {
        return person.profile.name;
    });
    document.title = 'Journalist ' + name + ' - The Digital Times';
  }
});

Router.route('/j/:username',{
  template: 'dashboard',
  waitOn: function() {
    return Meteor.subscribe('articlesUser', this.params.username, "normal") && Meteor.subscribe('userPage', this.params.username, "normal");
  },
  fastRender: true,
  data: function(){
    var currentProfile = this.params.username;
    var thisUser = Meteor.users.findOne({username: currentProfile});
    if (!thisUser) {
      this.render("notFound");
    } else {
      return thisUser;
    }
  },
  onAfterAction: function() {
    var name = this.data().profile.name + ' ' + this.data().profile.lastname;
    document.title = 'Journalist ' + name + ' - The Digital Times';
  }
});

Router.route('/tech');
Router.route('/tech/:_id',{
  template: 'articlePage',
  data: function(){
    var currentArticle = this.params._id;
    return Articles.findOne({_id: currentArticle})
  }
});

Router.route('/editProfile', {
  template: 'editProfile',
  onAfterAction: function() {
    document.title = 'Edit Profile - The Digital Times';
  }
});

Router.route('/terms', {
  template: 'terms',
  onAfterAction: function() {
    document.title = 'Terms and Conditions - The Digital Times';
  }
});

Router.route('/privacy', {
  template: 'privacy',
  onAfterAction: function() {
    document.title = 'Privacy Policy - The Digital Times';
  }
});

Router.route('/community-guidelines', {
  template: 'communityGuidelines',
  onAfterAction: function() {
    document.title = 'Community Guidelines - The Digital Times';
  }
});

Router.route('/tips', {
  template: 'tips',
  onAfterAction: function() {
    document.title = 'Tips and Tricks - The Digital Times';
  }
});
/* Chatting
Router.route('/debate');
Router.route('/chatRoom');
Router.route('/chatRoom2');
Router.route('/chatRoom3');
Router.route('/chatRoom4');*/

/* route to entertainment */
Router.route('/entertain', {
  waitOn: function() {
    return Meteor.subscribe('articlesSubject', 10, "entertain");
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'Entertainment - The Digital Times';
  }
});
Router.route('/entertain/:_id',{
  template: 'articlePage',
  waitOn: function() {
    return Meteor.subscribe('articlesPage', this.params._id, "entertain");
  },
  fastRender: true,
  data: function(){
    var currentArticle = this.params._id;
    var entertainArticle = Articles.findOne({_id: currentArticle});
    if (!entertainArticle) {
      this.render("notFound");
    } else {
      return entertainArticle;
    }
  },
  onAfterAction: function() {
    var post;
    if (!Meteor.isClient) {
      return;
    }
    post = this.data();
    SEO.set({
      title: post.title,
      meta: {
        'description'  : post.subtitle,
        'twitter:card' : "summary_large_image",
        'twitter:site' : "@realtdt",
        'twitter:image': post.imglink
      },
      og: {
        'title'      : post.title,
        'description': post.subtitle,
        'image'      : post.imglink
      }
    });
  }
});

Router.route('/politics', {
  waitOn: function() {
    return Meteor.subscribe('articlesSubject', 10, "politics");
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'Politics - The Digital Times';
  }
});

Router.route('/politics/:_id',{
  template: 'articlePage',
  waitOn: function() {
    return Meteor.subscribe('articlesPage', this.params._id, "politics");
  },
  fastRender: true,
  data: function(){
    var currentArticle = this.params._id;
    var politicalArticle = Articles.findOne({_id: currentArticle});
    if (!politicalArticle) {
      this.render("notFound");
    } else {
      return politicalArticle;
    }
  },
  onAfterAction: function() {
    var post;
    if (!Meteor.isClient) {
      return;
    }
    post = this.data();
    SEO.set({
      title: post.title,
      meta: {
        'description'  : post.subtitle,
        'twitter:card' : "summary_large_image",
        'twitter:site' : "@realtdt",
        'twitter:image': post.imglink
      },
      og: {
        'title'      : post.title,
        'description': post.subtitle,
        'image'      : post.imglink
      }
    });
  }
});

Router.route('/local',{
  name: 'local',
  template: 'local',
  waitOn: function() {
    return Meteor.subscribe('articlesSubject', 10, "local");
  },
  fastRender: true,
  onAfterAction: function() {
    var currentCity = Session.get("city");
    document.title = currentCity + ' Local - The Digital Times';
  }
});

Router.route('/local/:_id',{
    template: 'articlePage',
    waitOn: function() {
      return Meteor.subscribe('articlesPage', this.params._id, "local");
    },
    fastRender: true,
    data: function(){
      console.log(this.params);
      var currentArticle = this.params._id;
      var localArticle = Articles.findOne({_id: currentArticle});
      if (!localArticle) {
        this.render("notFound");
      } else {
        return localArticle;
      }
    },
    onAfterAction: function() {
      var post;
      if (!Meteor.isClient) {
        return;
      }
      post = this.data();
      SEO.set({
        title: post.title,
        meta: {
          'description'  : post.subtitle,
          'twitter:card' : "summary_large_image",
          'twitter:site' : "@realtdt",
          'twitter:image': post.imglink
        },
        og: {
          'title'      : post.title,
          'description': post.subtitle,
          'image'      : post.imglink
        }
      });
    }
});

Router.route('/mysubs',{
  name: 'mysubs',
  path: '/mysubs/',
  template: 'mysubs',
  waitOn: function() {
    return Meteor.subscribe('articlesInfinite');
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'Subcriptions - The Digital Times';
  }
});

Router.route('/mydrafts',{
  name: 'mydrafts',
  path: '/mydrafts/',
  template: 'mydrafts',
  waitOn: function() {
    return Meteor.subscribe('articlesInfinite');
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'My Drafts - The Digital Times';
  }
});

Router.route('/admin',{
  template: 'admin'
});

Router.route('/life', {
  waitOn: function() {
    return Meteor.subscribe('articlesSubject', 10, "life");
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'Life - The Digital Times';
  }
});
Router.route('/life/:_id',{
  template: 'articlePage',
  waitOn: function() {
    return Meteor.subscribe('articlesPage', this.params._id, "life");
  },
  fastRender: true,
  data: function(){
    var currentArticle = this.params._id;
    var lifeArticle = Articles.findOne({_id: currentArticle});
    if (!lifeArticle) {
      this.render("notFound");
    } else {
      return lifeArticle;
    }
  },
  onAfterAction: function() {
    var post;
    if (!Meteor.isClient) {
      return;
    }
    post = this.data();
    SEO.set({
      title: post.title,
      meta: {
        'description'  : post.subtitle,
        'twitter:card' : "summary_large_image",
        'twitter:site' : "@realtdt",
        'twitter:image': post.imglink
      },
      og: {
        'title'      : post.title,
        'description': post.subtitle,
        'image'      : post.imglink
      }
    });
  }
});

Router.route('/sports', {
  waitOn: function() {
    return Meteor.subscribe('articlesSubject', 10, "sports");
  },
  fastRender: true,
  onAfterAction: function() {
    document.title = 'Sports - The Digital Times';
  }
});
Router.route('/sports/:_id',{
  template: 'articlePage',
  waitOn: function() {
    return Meteor.subscribe('articlesPage', this.params._id, "sports");
  },
  fastRender: true,
  data: function(){
    var currentArticle = this.params._id;
    var sportArticle = Articles.findOne({_id: currentArticle});
    if (!sportArticle) {
      this.render("notFound");
    } else {
      return sportArticle;
    }
  },
  onAfterAction: function() {
    /*var currentArticle = this.params._id;
    var art = Articles.find({_id: currentArticle});
    var title = art.map(function (piece) {
          return piece.title;
    });*/
    var post;
    if (!Meteor.isClient) {
      return;
    }
    post = this.data();
    SEO.set({
      title: post.title,
      meta: {
        'description'  : post.subtitle,
        'twitter:card' : "summary_large_image",
        'twitter:site' : "@realtdt",
        'twitter:image': post.imglink
      },
      og: {
        'title'      : post.title,
        'description': post.subtitle,
        'image'      : post.imglink
      }
    });
  }
});

Router.route('/search');
