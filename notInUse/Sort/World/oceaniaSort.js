/*Europe Sort*/
if (Meteor.isClient) {
  /*Sort of viral!*/
  Template.oceaniaRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "oceania"}, {sort: {createdAt: -1}});
    }
  });

  Template.oceania24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "oceania", createdAt: {$gt : yesterday}}, {sort: {likes: -1}});
    }
  });

  Template.oceaniaWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "oceania", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.oceaniaMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "oceania", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.oceaniaYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "oceania", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}});
    }
  });

  Template.oceaniaAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "europe"}, {sort: {likes: -1}});
    }
  });
}