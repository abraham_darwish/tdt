/*America Sort*/
if (Meteor.isClient) {
  Template.americasSideSection.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "americas"}, {limit:6});
    }
  });
  /*Sort of viral!*/
  /*Sort of AmericasRecent*/
  Template.AmericasRecent.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AmericasRecent.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AmericasRecent.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AmericasRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "americas"}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of Americas24*/
  Template.Americas24.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.Americas24.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.Americas24.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.Americas24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "americas", createdAt: {$gt : yesterday}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasWeek*/
  Template.AmericasWeek.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AmericasWeek.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AmericasWeek.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AmericasWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "americas", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasMonth*/
  Template.AmericasMonth.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AmericasMonth.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AmericasMonth.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AmericasMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "americas", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasYear*/
  Template.AmericasYear.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AmericasYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AmericasYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AmericasYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "americas", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasAll*/
  Template.AmericasAll.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AmericasAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AmericasAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AmericasAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "americas"}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
}