/*Europe Sort*/
if (Meteor.isClient) {
  Template.europeSideSection.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "europe"}, {limit:6});
    }
  });
  /*Sort of viral!*/
  /*Sort of europeRecent*/
  Template.europeRecent.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europeRecent.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europeRecent.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europeRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "europe"}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of europe24*/
  Template.europe24.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europe24.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europe24.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europe24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "europe", createdAt: {$gt : yesterday}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of europeWeek*/
  Template.europeWeek.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europeWeek.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europeWeek.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europeWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "europe", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of europeMonth*/
  Template.europeMonth.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europeMonth.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europeMonth.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europeMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "europe", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of europeYear*/
  Template.europeYear.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europeYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europeYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europeYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "europe", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of europeAll*/
  Template.europeAll.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.europeAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.europeAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.europeAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "europe"}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
}