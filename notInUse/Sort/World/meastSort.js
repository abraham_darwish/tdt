/*Europe Sort*/
if (Meteor.isClient) {
  /*Sort of viral!*/
  Template.meastRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "meast"}, {sort: {createdAt: -1}});
    }
  });

  Template.meast24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "meast", createdAt: {$gt : yesterday}}, {sort: {likes: -1}});
    }
  });

  Template.meastWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "meast", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.meastMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "meast", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.meastYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "meast", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}});
    }
  });

  Template.meastAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "meast"}, {sort: {likes: -1}});
    }
  });
}