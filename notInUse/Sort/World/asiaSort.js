/*Asia Sort*/
if (Meteor.isClient) {
  Template.asiaSideSection.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "asia"}, {limit:6});
    }
  });
  /*Sort of viral!*/
  /*Sort of AmericasRecent*/
  Template.AsiaRecent.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AsiaRecent.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AsiaRecent.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AsiaRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "asia"}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of Americas24*/
  Template.Asia24.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.Asia24.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.Asia24.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.Asia24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "asia", createdAt: {$gt : yesterday}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasWeek*/
  Template.AsiaWeek.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AsiaWeek.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AsiaWeek.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AsiaWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "asia", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasRecent*/
  Template.AsiaMonth.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AsiaMonth.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AsiaMonth.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AsiaMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "asia", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasYear*/
  Template.AsiaYear.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AsiaYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AsiaYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AsiaYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "asia", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*Sort of AmericasRecent*/
  Template.AsiaAll.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AsiaAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AsiaAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AsiaAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "asia"}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
}