/*Section Sort Viral*/
if (Meteor.isClient) {
  /* counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  }); 
  */

  /*Sort of viral!*/
  Template.worldSideSection.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false}, {limit:6});
    }
  });
  /*sectionBody Day*/
  Template.sectionBodyDay.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.sectionBodyDay.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sectionBodyDay.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sectionBodyDay.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, createdAt: {$gt : yesterday}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
  /*sectionBodyWeek*/
  Template.sectionBodyWeek.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.sectionBodyWeek.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sectionBodyWeek.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sectionBodyWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*sectionBodyYear*/
  Template.sectionBodyYear.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.sectionBodyYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sectionBodyYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });  
  Template.sectionBodyYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*sectionBodyAll*/
  Template.sectionBodyAll.created = function() {
    Session.set('limit', 5);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.sectionBodyAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.sectionBodyAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.sectionBodyAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
}