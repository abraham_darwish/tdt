/*Africa Sort*/
if (Meteor.isClient) {
  /* counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  }); 
  */
  /*Sort of viral!*/
  /*AfricaRecent*/
  Template.AfricaRecent.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AfricaRecent.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AfricaRecent.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AfricaRecent.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false, wldcategory: "africa"}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });

  /*Africa24 Day*/
  Template.Africa24.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.Africa24.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.Africa24.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.Africa24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "world", draft: false, wldcategory: "africa", createdAt: {$gt : yesterday}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*AfricaRecent*/
  Template.AfricaWeek.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AfricaWeek.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AfricaWeek.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AfricaWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "world", draft: false, wldcategory: "africa", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*AfricaMonth*/
  Template.AfricaMonth.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AfricaMonth.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AfricaMonth.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AfricaMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "world", draft: false, wldcategory: "africa", createdAt: {$gt : weekAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*AfricaYear*/
  Template.AfricaYear.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AfricaYear.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AfricaYear.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AfricaYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "world", draft: false, wldcategory: "africa", createdAt: {$gt : yearAgo}}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });

  /*AfricaAll*/
  Template.AfricaAll.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.AfricaAll.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.AfricaAll.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });
  Template.AfricaAll.helpers({
    articles: function(){ 
      return Articles.find({category: "world", draft: false, wldcategory: "africa"}, {sort: {likes: -1}, limit: Session.get('limit')});
    }
  });
}