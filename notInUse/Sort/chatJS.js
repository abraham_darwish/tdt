if (Meteor.isClient) {
	Template.messages.helpers({
		messages: function(){
			return Messages.find({}, {sort: {ts: -1}});
		}
	})

	Template.chatRoom.helpers({
		messages: function(room){
			return Messages.find({room: room}, {sort: {ts: 1}});
		}
	})

	Template.chatRoom.events({
		'submit form': function(event){
			event.preventDefault();

			if (! Meteor.user().profile.name) {
				var name = Meteor.user().username;
			} else {
				var name = Meteor.user().profile.name;
			}

			var message = document.getElementById('message').value;
			var room = 1;
			var state = 0;

			if (message.value !== '') {
				Meteor.call("sendMessage", name, message, room, state);
				document.getElementById('message').value = '';
				message.value = '';
			};
		},
		'click #emoji': function(event){
			window.open('http://emoji.codes/');
		}
	})

	Template.chatRoom2.helpers({
		messages: function(room){
			return Messages.find({room: room}, {sort: {ts: 1}});
		}
	})

	Template.chatRoom2.events({
		'submit form': function(event){
			event.preventDefault();

			if (! Meteor.user().profile.name) {
				var name = Meteor.user().username;
			} else {
				var name = Meteor.user().profile.name;
			}

			var message = document.getElementById('message').value;
			var room = 2;
			var state = 0;

			if (message.value !== '') {
				Meteor.call("sendMessage", name, message, room, state);
				document.getElementById('message').value = '';
				message.value = '';
			};
		},
		'click #emoji': function(event){
			window.open('http://emoji.codes/');
		}
	})


	Template.chatRoom3.helpers({
		messages: function(room){
			return Messages.find({room: room}, {sort: {ts: 1}});
		}
	})

	Template.chatRoom3.events({
		'submit form': function(event){
			event.preventDefault();

			if (! Meteor.user().profile.name) {
				var name = Meteor.user().username;
			} else {
				var name = Meteor.user().profile.name;
			}

			var message = document.getElementById('message').value;
			var room = 3;
			var state = 0;

			if (message.value !== '') {
				Meteor.call("sendMessage", name, message, room, state);
				document.getElementById('message').value = '';
				message.value = '';
			};
		},
		'click #emoji': function(event){
			window.open('http://emoji.codes/');
		}
	})

	Template.chatRoom4.helpers({
		messages: function(room){
			return Messages.find({room: room}, {sort: {ts: 1}});
		}
	})

	Template.chatRoom4.events({
		'submit form': function(event){
			event.preventDefault();

			if (! Meteor.user().profile.name) {
				var name = Meteor.user().username;
			} else {
				var name = Meteor.user().profile.name;
			}

			var message = document.getElementById('message').value;
			var room = 4;
			var state = 0;

			if (message.value !== '') {
				Meteor.call("sendMessage", name, message, room, state);
				document.getElementById('message').value = '';
				message.value = '';
			};
		},
		'click #emoji': function(event){
			window.open('http://emoji.codes/');
		}
	})

	Template.input.events({
		'submit form': function(event){
			event.preventDefault();

			if (! Meteor.user().profile.name) {
				var name = Meteor.user().username;
			} else {
				var name = Meteor.user().profile.name;
			}

			var message = document.getElementById('message').value;

			if (message.value !== '') {
				Meteor.call("sendMessage", name, message);
				document.getElementById('message').value = '';
				message.value = '';
			};
		}
	})
}