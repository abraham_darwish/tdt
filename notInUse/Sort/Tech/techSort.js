if (Meteor.isClient) {
  /*Sort of viral!*/
  Template.techSideSection.helpers({
  	articles: function(){
      return Articles.find({category: "tech", draft: false},{sort: {limit:6}});
    }
  });

  Template.techRecent.helpers({
    articles: function(){
      return Articles.find({category: "tech", draft: false}, {sort: {createdAt: -1}});
    }
  });

  Template.tech24.helpers({
    articles: function(){
      var yesterday = new Date((new Date).setDate(new Date().getDate() - 1)); //1 is the number of days
      return Articles.find({category: "tech", draft: false, createdAt: {$gt : yesterday}}, {sort: {likes: -1}});
    }
  });

  Template.techWeek.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 7)); 
      return Articles.find({category: "tech", draft: false, createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.techMonth.helpers({
    articles: function(){
      var weekAgo = new Date((new Date).setDate(new Date().getDate() - 30)); 
      return Articles.find({category: "tech", draft: false, createdAt: {$gt : weekAgo}}, {sort: {likes: -1}});
    }
  });

  Template.techYear.helpers({
    articles: function(){
      var yearAgo = new Date((new Date).setDate(new Date().getDate() - 365)); 
      return Articles.find({category: "tech", draft: false, createdAt: {$gt : yearAgo}}, {sort: {likes: -1}});
    }
  });

  Template.techAll.helpers({
    articles: function(){ 
      return Articles.find({category: "tech", draft: false}, {sort: {likes: -1}});
    }
  });
}