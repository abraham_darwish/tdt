if (Meteor.isClient) {
  /*window.onload = function(){
    var startPos;
    var geoOptions = {
      maximumAge: 5*60*1000,
      timeout: 10*1000,
    }
    var geoSuccess = function(position){
      startPos = position;
      document.getElementById('startLat').innerHTML = startPos.coords.latitude;
      document.getElementById('startLon').innerHTML = startPos.coords.longitude;
    };

    navigator.geolocation.getCurrentPosition(geoSuccess, geoOptions);
  }*/

  incrementLimit = function(inc){
    inc = 10;
    newLimit = Session.get('limit') + inc;
    Session.set('limit', newLimit);
  }

  Template.newWorld.created = function() {
    Session.set('limit', 10);

    Deps.autorun(function(){
      Meteor.subscribe('articlesInfinite', Session.get('limit'));
    });
  }
  Template.newWorld.rendered = function(){
    $(window).scroll(function(){
      if ($(window).scrollTop() + $(window).height() > $(document).height - 100) {incrementLimit()}
    });
  }
  Template.newWorld.events({
    'click #loadMore':function(evt){
      incrementLimit();
    }
  });

  Template.newWorld.helpers({
    articles: function(){
      return Articles.find({category: "world", draft: false}, {sort: {createdAt: -1}, limit: Session.get('limit')});
    }
  });

  /*Scroll chat to bottom
  Template.chatRoom.helpers({
    scroll: function(){
      jQuery(document).ready(function($){
        $("#debateAll").scrollTop($("#debateAll")[0].scrollHeight);
        $("#debateViral").scrollTop($("#debateViral")[0].scrollHeight);
      });
    }
  })*/
  /*
  Template.addList.events({
    'submit form': function(event){
      event.preventDefault();
      var listName = $('[name=listName]').val();
      Lists.insert({
        name: listName
      });
      $('[name=listName]').val('');
    }
  });

  Template.lists.helpers({
    'list': function(){
      return Lists.find({}, {sort:{name: 1}});
    }
  });*/

  /*Comments.ui.config({
    template: 'bootstrap'
  });*/
}
